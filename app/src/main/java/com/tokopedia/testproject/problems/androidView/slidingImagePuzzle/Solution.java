package com.tokopedia.testproject.problems.androidView.slidingImagePuzzle;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.tokopedia.testproject.GlideApp;
import com.tokopedia.testproject.R;
import com.tokopedia.testproject.UtilKt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class Solution {
    public static Observable<List<Bitmap>> sliceTo4x4(final Context context, final String imageUrl) {
        return Observable.create(new ObservableOnSubscribe<List<Bitmap>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Bitmap>> emitter) {
                final ArrayList<Bitmap> bitmapList = new ArrayList<>();
                try {
                    Bitmap resource = GlideApp.with(context).asBitmap().load(imageUrl).into(400,400).get();
                    Bitmap [][] result = splitBitmap(resource, 4, 4);
                    for (int i = 0; i< result.length; i++){
                        for(int j =0; j < result[i].length; j++){
                            bitmapList.add(result[j][i]);
                        }
                    }
                    emitter.onNext(bitmapList);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    emitter.onError(e);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    emitter.onError(e);
                }
            }
        });
    }
    private static Bitmap[][] splitBitmap(Bitmap bitmap, int xCount, int yCount) {
        // Allocate a two dimensional array to hold the individual images.
        Bitmap[][] bitmaps = new Bitmap[xCount][yCount];
        int width, height;

        width = bitmap.getWidth() / xCount;
        height = bitmap.getHeight() / yCount;

        for(int x = 0; x < xCount; ++x) {
            for(int y = 0; y < yCount; ++y) {
                bitmaps[x][y] = Bitmap.createBitmap(bitmap, x * width, y * height, width, height);
            }
        }
        return bitmaps;
    }
}
