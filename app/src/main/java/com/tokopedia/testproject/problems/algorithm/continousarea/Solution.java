package com.tokopedia.testproject.problems.algorithm.continousarea;

import android.util.MutableInt;

import java.util.Stack;

/**
 * Created by hendry on 18/01/19.
 */
public class Solution {
    private static int ROW;
    private static int COL;
    public static int maxContinuousArea(int[][] matrix) {
        ROW = matrix.length;
        COL = matrix[0].length;
        Stack<Integer> listTarget = new Stack<>();
        boolean [][] visited = new boolean[ROW][COL];
        for(int i = 0; i < ROW; i++){
            for (int j = 0; j < COL; j++){
                if(!listTarget.contains(matrix[i][j])){
                    listTarget.add(matrix[i][j]);
                }
                visited[i][j] = false;
            }
        }
        int result = 0;
        int target = -1;
        while (!listTarget.isEmpty()){
            target = listTarget.pop();
            for(int i = 0; i< ROW; i++){
                for(int j = 0; j < COL; j++){
                    if(matrix[i][j] == target && !visited[i][j]){
                        MaximumContinuous count = new MaximumContinuous(1);
                        searchContinuousArea(target, matrix, i, j, visited, count);
                        result = Math.max(result, count.value);
                    }
                }
            }
        }

        return result;
    }

    private static boolean isSafe(int target, int [][] matrix, int row, int col, boolean [][] visited){
        return (row >= 0) && row < ROW && col >= 0 && col < COL && matrix[row][col] == target && !visited[row][col];
    }

    private static void searchContinuousArea(int target, int [][] matrix, int row, int col, boolean[][] visited, MaximumContinuous count){
        int rowMove[] = {0, -1, 0, 1};
        int colMove[] = {-1, 0, 1, 0};

        visited[row][col] = true;

        for(int k =0; k < 4; k++){
            int currentValue = matrix[row][col];
            if(isSafe(target, matrix, row+rowMove[k], col+colMove[k], visited)){
                int nextValue = matrix[row + rowMove[k]][col + colMove[k]];
                count.value = count.value + 1;
                searchContinuousArea(target, matrix, row + rowMove[k], col + colMove[k], visited, count);
            }
        }
    }


    static class MaximumContinuous{
        int value;
        MaximumContinuous(int value){
            this.value = value;
        }
    }
}
