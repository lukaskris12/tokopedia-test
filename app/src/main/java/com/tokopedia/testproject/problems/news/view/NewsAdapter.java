package com.tokopedia.testproject.problems.news.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tokopedia.testproject.GlideApp;
import com.tokopedia.testproject.R;
import com.tokopedia.testproject.UtilKt;
import com.tokopedia.testproject.problems.news.model.Article;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Object> articleList;

    private final int HEADER = 2;
    private final int ARTICLE = 5;

    NewsAdapter(List<Object> articleList) {
        setArticleList(articleList);
    }

    void setArticleList(List<Object> articleList) {
        if (articleList == null) {
            this.articleList = new ArrayList<>();
        } else {
            this.articleList = articleList;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return articleList.get(position) instanceof Article ? ARTICLE : HEADER;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder;

        if(viewType == ARTICLE){
            viewHolder = new NewsViewHolder(
                    LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news, viewGroup, false)
            );
        }else{
            viewHolder = new HeaderViewHolder(
                    LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_header, viewGroup, false)
            );
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof HeaderViewHolder) ((HeaderViewHolder) viewHolder).bind((String) articleList.get(i));
        else if(viewHolder instanceof NewsViewHolder) ((NewsViewHolder) viewHolder).bind((Article) articleList.get(i));
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView tvTitle;
        TextView tvDescription;
        TextView tvDate;

        NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvDate = itemView.findViewById(R.id.tvDate);
        }

        void bind(Article article) {
            GlideApp.with(itemView).load(article.getUrlToImage()).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
            tvTitle.setText(article.getTitle());
            tvDate.setText(UtilKt.toLocalDate(article.getPublishedAt()));
            tvDescription.setText(article.getDescription());
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder{
        TextView header;

        HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.header);
        }

        void bind(String header){
            this.header.setText(header);
        }
    }
}
