package com.tokopedia.testproject.problems.algorithm.waterJug;

import java.util.Vector;

public class Solution {

    public static int EMPTY = 1;
    public static int FILL = 2;
    public static int POUR = 3;

    public static int minimalPourWaterJug(int jug1, int jug2, int target) {
        Vector<Node> open = new Vector<>();
        open.add(new Node(0,0, 0, 0,null));
        int pourCount = 0;
        while (open.size() > 0){
            Node node = open.remove(0);
            if(node.x == target || node.y == target) {
                for ( Node n : node.getPath()) {
                    if(n.action == POUR) pourCount ++;
                }
                return pourCount;
            }else{
                Vector<Node> successors = getSuccessors(node, jug1, jug2);
                for(int i=0; i < successors.size(); i++){
                    Node child = successors.get(i);
                    if (!node.getPath().contains((Object)child)){
                        open.add(child);
                    }
                }
            }
        }
        return pourCount;
    }

    public static Vector<Node> getPath(int jug1, int jug2, int target){
        Vector<Node> open = new Vector<>();
        open.add(new Node(0,0, 0, 0,null));

        while (open.size() > 0){
            Node node = open.remove(0);
            if(node.x == target || node.y == target) {
                return node.getPath();
            }else{
                Vector<Node> successors = getSuccessors(node, jug1, jug2);
                for(int i=0; i < successors.size(); i++){
                    Node child = successors.get(i);
                    if (!node.getPath().contains((Object)child)){
                        open.add(child);
                    }
                }
            }
        }
        return null;
    }

    public static Vector<Node> getSuccessors(Node parent, int jug1, int jug2){
        int x = parent.x, y = parent.y;
        Vector<Node> successors = new Vector<>();
        if(x < jug1){
            successors.add(new Node(jug1, y, FILL, 1, parent));
        }
        if(y < jug2){
            successors.add(new Node(x, jug2, FILL, 2, parent));
        }
        if(x > 0){
            successors.add(new Node(0, y, EMPTY, 1, parent));
        }
        if(y > 0){
            successors.add(new Node(x, 0, EMPTY, 2, parent));
        }
        if (x + y >= jug1 && y > 0){
            successors.add(new Node(jug1, y - (jug1-x), POUR, 1, parent));
        }
        if(x + y >= jug2 && x > 0){
            successors.add(new Node(x - (jug2 - y), jug2, POUR, 2, parent));
        }
        if(x + y <= jug1 && y > 0){
            successors.add(new Node(x+y, 0, POUR, 1, parent));
        }
        if(x + y <= jug2 && x>0){
            successors.add(new Node(0, x+y, POUR, 2, parent));
        }
        return(successors);
    }
}
