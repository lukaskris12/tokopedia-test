package com.tokopedia.testproject.problems.news.network;

import com.tokopedia.testproject.problems.news.model.NewsResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsService {
    @GET("everything")
    Observable<NewsResult> getEverything(
            @Query("q") String query,
            @Query("language") String language,
            @Query("page") Integer page,
            @Query("pageSize") Integer pageSize,
            @Query("sortBy") String sort
    );

    @GET("top-headlines")
    Observable<NewsResult> getHeadlines(
            @Query("q") String query,
            @Query("language") String language,
            @Query("pageSize") Integer pageSize
    );
}
