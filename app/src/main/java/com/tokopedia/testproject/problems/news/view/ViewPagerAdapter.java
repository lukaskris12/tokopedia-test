package com.tokopedia.testproject.problems.news.view;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tokopedia.testproject.R;
import com.tokopedia.testproject.problems.news.model.Article;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {

    private ArrayList<Article> headline;
    private Context context;
    private LayoutInflater inflater;

    ViewPagerAdapter(Context context){
        this.context = context;
        headline = new ArrayList<>();
        inflater = LayoutInflater.from(context);
    }

    void submitList(List<Article> headline){
        this.headline.clear();
        this.headline.addAll(headline);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return headline.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View bannerView = inflater.inflate(R.layout.item_banner, view, false);

        assert bannerView != null;
        final ImageView imageView = bannerView.findViewById(R.id.image);
        final TextView title = bannerView.findViewById(R.id.title);

        Article article = headline.get(position);
        title.setText(article.getTitle());
        Glide.with(imageView).load(article.getUrlToImage()).into(imageView);

        view.addView(bannerView, 0);

        return bannerView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
