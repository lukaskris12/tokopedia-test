package com.tokopedia.testproject.problems.algorithm.waterJug;

import java.util.Vector;

public class Node{
    int x;
    int y;
    int action;
    int target;
    Node parent = null;
    Node(int amount1, int amount2, int action, int target, Node parent){
        this.x = amount1;
        this.y = amount2;
        this.target = target;
        this.action = action;
        this.parent = parent;
    }

    @Override
    public boolean equals(Object node) {
        return(((Node)node).x == x && ((Node)node).y == y);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getAction() {
        return action;
    }

    public int getTarget() {
        return target;
    }

    public Node getParent() {
        return parent;
    }

    Vector<Node> getPath(Vector<Node> v){
        v.insertElementAt(this, 0);
        if(parent != null) v = parent.getPath(v);
        return (v);
    }

    Vector<Node> getPath(){ return(getPath(new Vector<Node>())); }
}
