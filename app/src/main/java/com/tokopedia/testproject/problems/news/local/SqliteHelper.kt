package com.tokopedia.testproject.problems.news.local

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*

class SqliteHelper(context: Context) : ManagedSQLiteOpenHelper(context, "news", null, 1){
    companion object {
        private var instance: SqliteHelper? = null

        @Synchronized
        fun getInstance(context: Context): SqliteHelper{
            if (instance == null) {
                instance = SqliteHelper(context.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable("Article", true,
                "id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                "source_id" to TEXT,
                "source_name" to TEXT,
                "author" to TEXT,
                "title" to TEXT,
                "description" to TEXT,
                "url" to TEXT,
                "url_to_image" to TEXT,
                "published_at" to TEXT,
                "content" to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable("Article", true)
    }
}

val Context.database: SqliteHelper
    get() = SqliteHelper.getInstance(applicationContext)