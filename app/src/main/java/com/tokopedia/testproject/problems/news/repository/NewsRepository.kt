package com.tokopedia.testproject.problems.news.repository

import android.annotation.SuppressLint
import android.content.Context
import com.tokopedia.testproject.problems.news.local.NewsLocalDataSource
import com.tokopedia.testproject.problems.news.model.Article
import com.tokopedia.testproject.problems.news.network.NewsDataSource
import io.reactivex.Emitter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NewsRepository {

    companion object {

        fun load(context: Context, filter: String?, offset: Int, limit: Int) = Observable.create<List<Article>>{ emitter ->
            NewsDataSource.getService().getEverything(filter,"en", offset, limit, "publishedAt").subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                it.articles?.let { articles ->
                                    NewsLocalDataSource.create(context, articles)
                                            .subscribeOn(Schedulers.io())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(
                                                    {
                                                        loadLocal(context, filter, offset, limit, emitter)
                                                    },
                                                    {
                                                        loadLocal(context, filter, offset, limit, emitter)
                                                    }
                                            )
                                }
                            },
                            {
                                loadLocal(context, filter, offset, limit, emitter)
                            }
                    )
        }

        @SuppressLint("CheckResult")
        private fun loadLocal(context: Context, filter: String?, offset: Int, limit: Int, emitter: Emitter<List<Article>>) {
            NewsLocalDataSource.load(context, filter ?: "", (offset - 1) * limit, limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                emitter.onNext(it)
                            },
                            {
                                emitter.onError(it)
                            },
                            {
                                emitter.onComplete()
                            }
                    )

        }

        fun loadLocal(context: Context, filter: String?, offset: Int, limit: Int) = Observable.create<List<Article>>{emitter->
            NewsLocalDataSource.load(context, filter ?: "", (offset - 1) * limit, limit)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            {
                                emitter.onNext(it)
                            },
                            {
                                emitter.onError(it)
                            },
                            {
                                emitter.onComplete()
                            }
                    )

        }
    }
}