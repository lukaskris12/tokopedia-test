package com.tokopedia.testproject.problems.news.presenter;

import android.content.Context;
import android.util.Pair;

import com.tokopedia.testproject.UtilKt;
import com.tokopedia.testproject.problems.news.model.Article;
import com.tokopedia.testproject.problems.news.model.NewsResult;
import com.tokopedia.testproject.problems.news.network.NewsDataSource;
import com.tokopedia.testproject.problems.news.repository.NewsRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import kotlin.Unit;

/**
 * Created by hendry on 27/01/19.
 */
public class NewsPresenter {
    public final Integer LIMIT = 20;
    private CompositeDisposable composite = new CompositeDisposable();

    private Context context;
    private View view;
    private Pair<String, Integer> searchData;
    private List<Object> articles;

    public interface View {
        void onLoadingLoadMoreNews();

        void onLoadingGetHeadlines();

        void onLoadingGetNews();

        void onSuccessGetNews(List<Object> articleList);

        void onSuccessGetHeadlines(List<Article> headlineList);

        void onErrorGetNews(Throwable throwable);

        void onErrorGetHeadlines(Throwable throwable);
    }

    public NewsPresenter(Context context, NewsPresenter.View view) {
        this.context = context;
        this.view = view;
        if(articles == null) articles = new ArrayList<>();
        searchData = new Pair<>(null, 1);
    }

    public void getEverything() {
        String keyword = searchData.first;
        final int offset = searchData.second;

        if(offset == 1) view.onLoadingGetNews();
        else view.onLoadingLoadMoreNews();

        NewsRepository.Companion.load(context, keyword, offset, LIMIT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Article>>() {

                    @Override
                    public void onSubscribe(Disposable d) { }

                    @Override
                    public void onNext(List<Article> list) {
                        if(articles == null) articles = new ArrayList<>();

                        if(offset == 1 && !articles.isEmpty()) articles.clear();

                        if(!articles.isEmpty()){
                            Object article = articles.get(articles.size()-1);
                            if(article instanceof Article){
                                if(!list.isEmpty()) generateWithHeader(list, ((Article) article).getPublishedAt());
                            }
                        }
                        if(list != null && !list.isEmpty()){
                             generateWithHeader(list, null);
                             articles.addAll(list);
                        }
                        else searchData = new Pair<>(searchData.first, searchData.second - 1);

                        view.onSuccessGetNews(articles);

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorGetNews(e);
                    }

                    @Override
                    public void onComplete() { }
                });

//        NewsDataSource.getService().getEverything(keyword, offset, LIMIT, "publishedAt")
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<NewsResult>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                        composite.add(d);
//                    }
//
//                    @Override
//                    public void onNext(NewsResult newsResult) {
//                        if(articles == null) articles = new ArrayList<>();
//
//                        if(!articles.isEmpty()){
//                            Object article = articles.get(articles.size()-1);
//                            if(article instanceof Article){
//                                generateWithHeader(newsResult.getArticles(), ((Article) article).getPublishedAt());
//                            }
//                        }else{
//                            generateWithHeader(newsResult.getArticles(), null);
//                        }
//
//                        if(newsResult.getArticles() != null) articles.addAll(newsResult.getArticles());
//                        view.onSuccessGetNews(articles);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        view.onErrorGetNews(e);
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });
    }

    public void getHeadlines() {
        String keyword = searchData.first;
        view.onLoadingGetHeadlines();
        NewsDataSource.getService().getHeadlines(keyword,"en", 5)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NewsResult>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        composite.add(d);
                    }

                    @Override
                    public void onNext(NewsResult newsResult) {
                        view.onSuccessGetHeadlines(newsResult.getArticles());
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorGetHeadlines(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void search(String keyword) {
        if(articles != null) articles.clear();
        searchData = new Pair<>(keyword.isEmpty() ? "android" : keyword, 1);
        getEverything();
        getHeadlines();
    }

    public void reload() {
        searchData = new Pair<>(searchData.first, 1);
        if(articles != null) articles.clear();
        getEverything();
        getHeadlines();
    }

    public Unit loadMore(){
        searchData = new Pair<>(searchData.first, searchData.second + 1);
        getEverything();
        return null;
    }

    private void generateWithHeader(List<Article> articles, String previousHeader){
        if(previousHeader == null && !articles.isEmpty()){
            String date = UtilKt.toLocalDate(articles.get(0).getPublishedAt());
            this.articles.add(date);
            this.articles.add(articles.get(0));
            for(int i = 1; i < articles.size(); i++){
                Article article = articles.get(i);

                //Handle different header
                if(!this.articles.isEmpty()) {
                    String previousDate = UtilKt.toLocalDate(articles.get(i-1).getPublishedAt());
                    String currentDate = UtilKt.toLocalDate(article.getPublishedAt());
                    if(!previousDate.equals(currentDate)){
                        this.articles.add(currentDate);
                    }
                }

                this.articles.add(article);
            }
        }
        else{
            String previousDate;
            String currentDate;
            if(this.articles != null && this.articles.size() > 0){
                previousDate = UtilKt.toLocalDate(((Article) this.articles.get(this.articles.size() - 1)).getPublishedAt());
                currentDate = UtilKt.toLocalDate(articles.get(0).getPublishedAt());
                if(!currentDate.equals(previousDate)){
                    this.articles.add(currentDate);
                }
            }
            this.articles.add(articles.get(0));

            for(int i = 1; i < articles.size(); i++){
                Article article = articles.get(i);

                //Handle different header
                if(!this.articles.isEmpty()) {
                    previousDate = i == 0 ? previousHeader : UtilKt.toLocalDate(articles.get(i-1).getPublishedAt());
                    currentDate = UtilKt.toLocalDate(article.getPublishedAt());
                    if(!previousDate.equals(currentDate)){
                        this.articles.add(currentDate);
                    }
                }

                this.articles.add(article);
            }
        }

    }

    public void unsubscribe() {
        composite.dispose();
    }
}
