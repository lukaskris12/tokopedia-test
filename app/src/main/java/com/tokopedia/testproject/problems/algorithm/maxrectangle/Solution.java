package com.tokopedia.testproject.problems.algorithm.maxrectangle;


import java.util.Stack;

public class Solution {
    public static int maxRect(int[][] matrix) {
        int result = maxHistogram(0,0, matrix[0]);
        int R = matrix.length;
        int C = matrix[0].length;
        for(int i = 1; i< R; i++){
            for(int j = 0; j < C; j++){
                if(matrix[i][j] == 1) matrix[i][j] += matrix[i-1][j];
            }
            result = Math.max(result, maxHistogram(R, C, matrix[i]));
        }
        return result;
    }

    private static int maxHistogram(int R, int C, int row[]){
        Stack<Integer> result = new Stack<>();
        int top;
        int max_area = 0;
        int area = 0;
        int i = 0;
        while (i < C){
            if(result.empty() || row[result.peek()] <= row[i]){
                result.push(i++);
            }else{
                top = row[result.peek()];
                result.pop();
                area = top * i;
                if(!result.empty()){
                    area = top * (i-result.peek() - 1);
                }
                max_area = Math.max(area, max_area);
            }
        }

        while (!result.empty()){
            top = row[result.peek()];
            result.pop();
            area = top * i;
            if(!result.empty()) area = top * (i-result.peek() -1);
            max_area = Math.max(area, max_area);
        }
        return max_area;
    }
}
