package com.tokopedia.testproject.problems.androidView.slidingImagePuzzle;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.tokopedia.testproject.R;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SlidingImageGameActivity extends AppCompatActivity {
    public static final String X_IMAGE_URL = "x_image_url";
    public static final int GRID_NO = 4;
    private String imageUrl;
    CustomImageView[][] imageViews = new CustomImageView[4][4];
    private GridLayout gridLayout;

    public static Intent getIntent(Context context, String imageUrl) {
        Intent intent = new Intent(context, SlidingImageGameActivity.class);
        intent.putExtra(X_IMAGE_URL, imageUrl);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageUrl = getIntent().getStringExtra(X_IMAGE_URL);
        setContentView(R.layout.activity_sliding_image_game);
        gridLayout = findViewById(R.id.gridLayout);

        LayoutInflater inflater = LayoutInflater.from(this);
        for (int i = 0; i < GRID_NO; i++) {
            for (int j = 0; j < GRID_NO; j++) {
                final CustomImageView view = (CustomImageView) inflater.inflate(R.layout.item_image_sliding_image,
                        gridLayout, false);
                view.setOriginalPosition(i, j);
                imageViews[i][j] = view;
            }
        }
        imageViews[GRID_NO - 1][GRID_NO - 1].setBlankSpace(true);

        Solution.sliceTo4x4(this, imageUrl).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<List<Bitmap>>() {
            @Override
            public void onSubscribe(Disposable d) {}

            @Override
            public void onNext(List<Bitmap> bitmaps) {
                int counter = 0;
                int bitmapSize = bitmaps.size();
                for (int i = 0; i < GRID_NO; i++) {
                    for (int j = 0; j < GRID_NO; j++) {
                        if (counter >= bitmapSize - 1) break;
                        imageViews[i][j].setImageBitmap(bitmaps.get(counter));
                        counter++;
                    }
                    if (counter >= bitmapSize - 1) break;
                }

                shuffleMap();

                for(int i =0; i < 4; i++){
                    for(int j = 0; j < 4; j++){
                        final CustomImageView view = imageViews[i][j];
                        view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(!view.isBlankSpace()) {
                                    clickView(view);
                                }
                            }
                        });
                        gridLayout.addView(imageViews[i][j]);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(SlidingImageGameActivity.this,
                        e.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onComplete() {}
        });

        // TODO add handling for rotation to save the user input.
        // If the device is rotated, it should retain user's input, so user can continue the game.
    }

    void clickView(CustomImageView view){
        int rowMove[] = {0, -1, 0, 1};
        int colMove[] = {-1, 0, 1, 0};

        for (int k = 0; k < 4; k++) {
            int row = view.getCurrentX() + rowMove[k];
            int col = view.getCurrentY() + colMove[k];
            if (isSafe(row, col)) {
                CustomImageView blankView = imageViews[row][col];
                blankView.setImageBitmap(((BitmapDrawable) view.getDrawable()).getBitmap());
                blankView.setBlankSpace(false);
                view.setImageBitmap(null);
                view.setBlankSpace(true);
            }
        }

    }

    private boolean isSafe(int row, int col){
        return (row >= 0) && row < 4 && col >= 0 && col < 4 && imageViews[row][col].isBlankSpace();
    }

    private void shuffleMap(){
        int i = 15;
        while(i > 0){
            int j = (int) Math.floor(Math.random() * i);
            int xi = i % 4;
            int yi = (int) Math.floor(i / 4);
            int xj = j % 4;
            int yj = (int) Math.floor(j / 4);
            swapTiles(xi, yi, xj, yj);
            --i;
        }
        if(!isSolvable(GRID_NO, GRID_NO, getEmptyTile().getCurrentY() + 1)){
            if(getEmptyTile().getCurrentY() == 0 && getEmptyTile().getCurrentX() <= 1){
                swapTiles(GRID_NO - 2, GRID_NO -1, GRID_NO - 1, GRID_NO - 1);
            }else{
                swapTiles(0,0, 1,0);
            }
        }
    }

    private void swapTiles(int xi, int yi, int xj, int yj){
        CustomImageView temp = imageViews[xi][yi];
        CustomImageView temp2 = imageViews[xj][yj];
        imageViews[xi][yi] = temp2;
        imageViews[xj][yj] = temp;
        temp.setCurrentPosition(xj, yj);
        temp2.setCurrentPosition(xi, yi);
        if(isFinish()) Toast.makeText(this, "Sukses", Toast.LENGTH_SHORT).show();
    }

    private CustomImageView getEmptyTile(){
        for(int i=0; i<4; i++){
            for(int j=0; j<4; j++){
                if(imageViews[i][j].isBlankSpace()) return imageViews[i][j];
            }
        }
        return null;
    }

    private boolean isFinish(){
        for (int i = 0; i< GRID_NO; i++){
            for(int j=0; j< GRID_NO; j++){
                if(!imageViews[i][j].isFinish()) return false;
            }
        }
        return true;
    }

    private int countInversions(int i, int j){
        int inversions = 0;
        int tileNum = j * GRID_NO + i;
        int lastTile = GRID_NO * GRID_NO;
        int tileValue = imageViews[i][j].getCurrentY() * GRID_NO + imageViews[i][j].getCurrentX();
        for(int q = tileNum + 1; q< lastTile; q++){
            int k = q % GRID_NO;
            int l = (int) Math.floor(q / GRID_NO);
            int compValue = imageViews[k][l].getCurrentY() * GRID_NO + imageViews[k][l].getCurrentX();
            if(tileValue > compValue && tileValue != (lastTile -1)) ++inversions;
        }
        return inversions;
    }

    private int sumInversions(){
        int inversions = 0;
        for(int i = 0; i< GRID_NO; i++){
            for(int j = 0; j< GRID_NO; j++){
                inversions += countInversions(i,j);
            }
        }
        return inversions;
    }

    private boolean isSolvable(int width, int height, int emptyRow){
        if(width % 2 ==1) return (sumInversions() % 2 ==0);
        else return ((sumInversions() + height - emptyRow) % 2 == 0);
    }
}
