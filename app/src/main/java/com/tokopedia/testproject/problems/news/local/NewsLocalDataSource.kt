package com.tokopedia.testproject.problems.news.local

import android.content.Context
import com.tokopedia.testproject.problems.news.model.Article
import com.tokopedia.testproject.problems.news.model.Source
import com.tokopedia.testproject.toLocalDateTime
import io.reactivex.Observable
import org.jetbrains.anko.db.*
import java.util.*

class NewsLocalDataSource{

    companion object {
        fun create(context: Context, list: List<Article>) = Observable.create<Boolean>{ emitter ->
            context.database.use{
                beginTransaction()
                try {
                    list.forEach {
                        if(!checkIsExists(context, it)) {
                            this.insert("Article",
                                    "source_id" to it.source?.id,
                                    "source_name" to it.source?.name,
                                    "author" to it.author,
                                    "title" to it.title,
                                    "description" to it.description,
                                    "url" to it.url,
                                    "url_to_image" to it.urlToImage,
                                    "published_at" to toLocalDateTime(it.publishedAt),
                                    "content" to it.content
                            )
                        }
                    }
                    emitter.onNext(true)
                    setTransactionSuccessful()
                }catch (ex: Exception){
                    emitter.onError(ex)
                } finally {
                    emitter.onComplete()
                    endTransaction()
                }

            }
        }

        fun load(context: Context, filter: String, offset: Int, limit: Int) = Observable.create<List<Article>>{
            val db = context.database.readableDatabase
            val query = "select * from Article where title like '%$filter%' or description like '%$filter%' or content like '%$filter%' ORDER BY date(published_at) DESC LIMIT $offset,$limit"
            val cursor = db.rawQuery(query, null)
            val result = ArrayList<Article>()
            while (cursor.moveToNext()){
                val article = Article(
                        source = Source(id = cursor.getString(1) ?: "", name = cursor.getString(2) ?: ""),
                        author = cursor.getString(3) ?: "",
                        title = cursor.getString(4) ?: "",
                        description = cursor.getString(5) ?: "",
                        url = cursor.getString(6) ?: "",
                        urlToImage = cursor.getString(7) ?: "",
                        publishedAt = cursor.getString(8) ?: Date().toString(),
                        content = cursor.getString(9) ?: ""
                )
                result.add(article)
            }
//            val result = context.database.use {
//                select("Article")
//                        .whereArgs("(title like {filter}) or (description like {filter}) or (source_name like {filter}) or (url_to_image like {filter})", "filter" to "%$filter%")
//                        .limit(offset, limit)
//                        .orderBy("date(published_at)", SqlOrderDirection.DESC)
//                        .exec {
//                            parseList(object : MapRowParser<Article>{
//                                override fun parseRow(columns: Map<String, Any?>): Article {
//                                    return Article(
//                                            source = Source(id = columns.getValue("source_id").toString(), name = columns.getValue("source_name").toString()),
//                                            author = columns.getValue("author").toString(),
//                                            title = columns.getValue("title").toString(),
//                                            description = columns.getValue("description").toString(),
//                                            url = columns.getValue("url").toString(),
//                                            urlToImage = columns.getValue("url_to_image").toString(),
//                                            publishedAt = columns.getValue("published_at").toString(),
//                                            content = columns.getValue("content").toString()
//                                    )
//                                }
//                            })
//                        }
//
//            }
            it.onNext(result)
        }

        private fun checkIsExists(context: Context ,article: Article): Boolean{
            val selectedQuery = "select * from Article where title = '${article.title?.replace('\'', ' ')}' and published_at = '${toLocalDateTime(article.publishedAt)}' and source_name = '${article.source?.name}'"
            val db = context.database.readableDatabase
            try {
                val cursor = db.rawQuery(selectedQuery, null)
                if(cursor.count > 0){
                    cursor.close()
                    return true
                }
                cursor.close()
            }catch (ex: java.lang.Exception){}
            return false
        }
    }

}