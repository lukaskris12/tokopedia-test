package com.tokopedia.testproject.problems.androidView.slidingImagePuzzle;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

public class CustomImageView extends android.support.v7.widget.AppCompatImageView {
    private int currentX, currentY, originalX, originalY = -1;
    private boolean blankSpace = false;
    public CustomImageView(Context context) {
        super(context);
    }

    public CustomImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public int getCurrentX() {
        return currentX;
    }

    public void setCurrentX(int currentX) {
        this.currentX = currentX;
    }

    public int getCurrentY() {
        return currentY;
    }

    public void setOriginalPosition(int x, int y){
        this.originalX = x;
        this.originalY = y;
    }

    public void setCurrentPosition(int x, int y){
        this.currentX = x;
        this.currentY = y;
    }

    public void setCurrentY(int currentY) {
        this.currentY = currentY;
    }

    public int getOriginalX() {
        return originalX;
    }

    public void setOriginalX(int originalX) {
        this.originalX = originalX;
    }

    public int getOriginalY() {
        return originalY;
    }

    public void setOriginalY(int originalY) {
        this.originalY = originalY;
    }

    public boolean isFinish(){
        return originalX == currentX && originalY == currentY;
    }

    public boolean isBlankSpace() {
        return blankSpace;
    }

    public void setBlankSpace(boolean blankSpace) {
        this.blankSpace = blankSpace;
    }
}
