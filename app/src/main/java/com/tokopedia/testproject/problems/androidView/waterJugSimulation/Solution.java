package com.tokopedia.testproject.problems.androidView.waterJugSimulation;

import com.tokopedia.testproject.problems.algorithm.waterJug.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import com.tokopedia.testproject.problems.algorithm.waterJug.Solution.*;

import static com.tokopedia.testproject.problems.algorithm.waterJug.Solution.FILL;
import static com.tokopedia.testproject.problems.algorithm.waterJug.Solution.POUR;

public class Solution {
    public static List<WaterJugAction> simulateWaterJug(int jug1, int jug2, int target) {

        Vector<Node> result = com.tokopedia.testproject.problems.algorithm.waterJug.Solution.getPath(jug1, jug2, target);
        List<WaterJugAction> list = new ArrayList<>();

        for(Node node : result){
            if(node.getAction() != 0) {
                list.add(new WaterJugAction(
                        node.getAction() == FILL ? WaterJugActionEnum.FILL :
                                node.getAction() == POUR ? WaterJugActionEnum.POUR : WaterJugActionEnum.EMPTY,
                        node.getTarget()
                ));
            }
        }

//        list.add(new WaterJugAction(WaterJugActionEnum.FILL, 1));
//        list.add(new WaterJugAction(WaterJugActionEnum.POUR, 2));
//        list.add(new WaterJugAction(WaterJugActionEnum.FILL, 1));
//        list.add(new WaterJugAction(WaterJugActionEnum.POUR, 2));
//        list.add(new WaterJugAction(WaterJugActionEnum.EMPTY, 2));
//        list.add(new WaterJugAction(WaterJugActionEnum.POUR, 2));
//        list.add(new WaterJugAction(WaterJugActionEnum.FILL, 1));
//        list.add(new WaterJugAction(WaterJugActionEnum.POUR, 2));
        return list;
    }
}
