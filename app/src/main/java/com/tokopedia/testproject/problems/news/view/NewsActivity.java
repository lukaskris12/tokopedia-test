package com.tokopedia.testproject.problems.news.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tokopedia.testproject.R;
import com.tokopedia.testproject.problems.news.model.Article;
import com.tokopedia.testproject.problems.news.presenter.NewsPresenter;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import kotlin.Unit;
import kotlin.jvm.functions.Function3;
import me.relex.circleindicator.CircleIndicator;

public class NewsActivity extends AppCompatActivity implements com.tokopedia.testproject.problems.news.presenter.NewsPresenter.View {
    private ProgressBar loading;
    private ProgressBar loadingLoadMore;
    private RecyclerView recyclerView;
    private TextView message;
    private TextView tryAgain;
    private ViewPager viewPager;
    private CircleIndicator indicator;
    private EditText searchBox;

    private EndlessRecyclerViewScrollListener scrollListener;

    private NewsPresenter newsPresenter;
    private NewsAdapter newsAdapter;
    private ViewPagerAdapter headlinesAdapter;

    private Disposable disposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        newsPresenter = new NewsPresenter(this, this);
        newsAdapter = new NewsAdapter(null);

        recyclerView = findViewById(R.id.recyclerView);
        loading = findViewById(R.id.loading);
        loadingLoadMore = findViewById(R.id.loading_load_more);
        message = findViewById(R.id.message);
        tryAgain = findViewById(R.id.retry);
        viewPager = findViewById(R.id.view_pager);
        indicator = findViewById(R.id.indicator);
        searchBox = findViewById(R.id.search);

        NestedScrollView nestedScrollView = findViewById(R.id.scroll_view);

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        newsPresenter.loadMore();
                    }
                }
            }
        });


//        scrollListener = new EndlessRecyclerViewScrollListener(
//                recyclerView.getLayoutManager(),
//                newsPresenter.LIMIT - 8,
//                new Function3<Integer, Integer, RecyclerView, Unit>() {
//                    @Override
//                    public Unit invoke(Integer integer, Integer integer2, RecyclerView recyclerView) {
//                        return newsPresenter.loadMore();
//                    }
//                }
//        );

        recyclerView.setNestedScrollingEnabled(false);
//        recyclerView.addOnScrollListener(scrollListener);
        recyclerView.setAdapter(newsAdapter);

        initHeadlines();
        newsPresenter.search("android");

        initSearch();

        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newsPresenter.reload();
            }
        });
    }

    void initHeadlines(){
        headlinesAdapter = new ViewPagerAdapter(this);
        viewPager.setAdapter(headlinesAdapter);
        indicator.setViewPager(viewPager);
        headlinesAdapter.registerDataSetObserver(indicator.getDataSetObserver());
    }

    void initSearch(){
        disposable = createTextObservable(this, 500)
                .observeOn(Schedulers.io())
                .doOnNext(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                    }
                })
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<String>() {
                    @Override
                    public void accept(String s) throws Exception {
                        newsPresenter.search(s);
                    }
                });
    }



    @Override
    public void onLoadingGetNews() {
        onLoading();
    }

    @Override
    public void onLoadingLoadMoreNews() {
        onLoadingLoadMore();
    }

    @Override
    public void onLoadingGetHeadlines() {
        viewPager.setVisibility(View.GONE);
        indicator.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessGetHeadlines(List<Article> headlineList) {
        viewPager.setVisibility(View.VISIBLE);
        indicator.setVisibility(View.VISIBLE);
        headlinesAdapter.submitList(headlineList);
    }

    @Override
    public void onErrorGetHeadlines(Throwable throwable) {
        viewPager.setVisibility(View.GONE);
        indicator.setVisibility(View.GONE);
    }

    @Override
    public void onSuccessGetNews(List<Object> articleList) {
        onSuccess();
        newsAdapter.setArticleList(articleList);
        newsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onErrorGetNews(Throwable throwable) {
        onError(throwable.getMessage());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        newsPresenter.unsubscribe();
        disposable.dispose();
    }

    private void onLoading(){
        loading.setVisibility(View.VISIBLE);
        tryAgain.setVisibility(View.GONE);
        message.setText("");
    }

    private void onLoadingLoadMore(){
        loadingLoadMore.setVisibility(View.VISIBLE);
        tryAgain.setVisibility(View.GONE);
        message.setText("");
    }

    private void onSuccess(){
        loading.setVisibility(View.GONE);
        loadingLoadMore.setVisibility(View.GONE);
        tryAgain.setVisibility(View.GONE);
        message.setText("");
    }

    private void onError(String errorMessage){
        loading.setVisibility(View.GONE);
        loadingLoadMore.setVisibility(View.GONE);
        tryAgain.setVisibility(View.VISIBLE);
        message.setText(errorMessage);
    }

    private Observable<String> createTextObservable(final Context context, int debounceInMillis){
        return Observable.create(new ObservableOnSubscribe<String>() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void subscribe(final ObservableEmitter<String> emitter) throws Exception {
                final TextWatcher textWatcher = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        emitter.onNext(s.toString());
                        searchBox.setCompoundDrawablesWithIntrinsicBounds(
                                ContextCompat.getDrawable(context, R.drawable.ic_search_black_24dp),
                                null,
                                !searchBox.getText().toString().isEmpty() ? ContextCompat.getDrawable(context, R.drawable.ic_clear_black_24dp) : null,
                                null

                        );
                    }

                    @Override
                    public void afterTextChanged(Editable s) { }
                };

                searchBox.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if(rightDrawableClick(searchBox, event)){
                            if (searchBox.getText().toString().isEmpty()) {
                                searchBox.setText("");
                            } else {
                                searchBox.getText().clear();
                                searchBox.clearFocus();
                            }
                            newsPresenter.search("");
                            searchBox.setCompoundDrawablesWithIntrinsicBounds(searchBox.getCompoundDrawables()[0], searchBox.getCompoundDrawables()[1], null, searchBox.getCompoundDrawables()[3]);

                        }
                        return false;
                    }
                });
                searchBox.addTextChangedListener(textWatcher);


                emitter.setDisposable(new Disposable() {
                    boolean isDisposed = false;
                    @Override
                    public void dispose() {
                        searchBox.removeTextChangedListener(textWatcher);
                        isDisposed = true;
                    }

                    @Override
                    public boolean isDisposed() {
                        return isDisposed;
                    }
                });
            }
        }).debounce(debounceInMillis, TimeUnit.MILLISECONDS);
    }

    private boolean rightDrawableClick(EditText editText, MotionEvent motionEvent){
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            Drawable drawable = editText.getCompoundDrawables()[2];
            return drawable != null && motionEvent.getRawX() >= (editText.getRight() - drawable.getBounds().width());
        }
        return false;
    }
}
